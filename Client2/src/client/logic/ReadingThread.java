package client.logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

public class ReadingThread extends Thread {

    private JTextArea textArea;
    private JTextArea textAreaUlist;
    private BufferedReader in;
    private PrintWriter out;
    private Socket socket;
    private static final Logger LOGGER = Logger.getLogger(ReadingThread.class.getName());
   
    public ReadingThread(JTextArea textArea, JTextArea textArealist, Socket socket) {
        this.textArea = textArea;
        this.textAreaUlist = textArealist;
        this.socket = socket;

    }

    @Override
    public void run() {
        setInOut(socket);
        while (true) {
            try {                
                String line = in.readLine();                
                if (line == null) {
                    break;
                }                                
                if ((" bye".equals(line))||(!socket.isConnected())) {
                    close();
                    textArea.append("Сервер разорвал соединение!");
                    javax.swing.JOptionPane.showMessageDialog(null, "Сервер разорвал соединение!");
                    System.exit(0);
                }
                print(line);                

            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
                javax.swing.JOptionPane.showMessageDialog(null, "Возможно сервер не запущен ");
            }
        }
    }

    private void setInOut(Socket socket) {
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

    }

    private void print(String line) {

        switch (isMessageOrUserList(line)) {
            case 0:
                printClientMessage(line);
                break;
            case 1:
                printUserList(line);
                break;
            case 2:
                printServerMessage(line);
                break;
        }
    }

    private int isMessageOrUserList(String line) {
        if ("m".equals(line.substring(0, 1))) {
            return 0;
        }
        if ("UL".equals(line.substring(0, 2))) {
            return 1;
        }

        return 2;
    }

    private void printClientMessage(String line) {
        line = line.substring(1);
        textArea.append(line + "\n");
    }

    private void printUserList(String line) {
        line = line.substring(2);        
        textAreaUlist.setText("");
        String[] usList = line.split("#");
        for (int i = 0; i < usList.length; i++) {
            textAreaUlist.append(usList[i] + "\n");
        }
    }

    private void printServerMessage(String line) {
        javax.swing.JOptionPane.showMessageDialog(null, line);
    }

    private void close() {

        try {
            if (in != null) {
                in.close();
            }
            if (out != null){
                out.close();
            }
            if (socket != null){
                socket.close();
            }
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

    }

}
