/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package log;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.logging.Logger.getLogger;
import java.util.logging.SimpleFormatter;
import props.PropsClass;

/**
 *
 * @author top
 */
public class LogClass {
    
    public static void setFileLogger() {
               
        try {
            Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
            Logger rootLogger = Logger.getLogger("");
            Handler[] handlers = rootLogger.getHandlers();
            if (handlers[0] instanceof ConsoleHandler) {
                rootLogger.removeHandler(handlers[0]);
            }
            FileHandler fh = new FileHandler(new PropsClass().getLogFileName());
            fh.setFormatter(new SimpleFormatter());
            fh.setLevel(Level.INFO);
            logger = Logger.getLogger("");
            logger.setUseParentHandlers(false);
            logger.addHandler(fh);
        } catch (IOException ex) {
            throw new RuntimeException("Problems with creating the log files");
        } catch (SecurityException ex) {
            throw new RuntimeException("Problems with creating the log files");
        }

    }

}
