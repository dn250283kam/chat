/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package props;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author top
 */
public class PropsClass {

    private Properties props = new Properties();
    private BufferedReader propsIn;
    private static final Logger logger = Logger.getLogger(PropsClass.class.getName());
    

    public PropsClass() {
        assign();        
    }

    private void assign() {
        try {
            propsIn = new BufferedReader(new FileReader("ServerConfig.properties"));
            props.load(propsIn);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PropsClass.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PropsClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    public int getServerPort() {
        int serverPort = 1234;
        try {
            serverPort = Integer.parseInt(props.getProperty("port"));
            //return serverPort;
        } catch (NumberFormatException ex) {
            logger.severe("Не указан или неверно указан порт сервера. Будет установлено значение по умолчанию: 1234");

        }
        return serverPort;
    }

    public int getLimitUsers() {
        int limitUsers = 9999;
        try {
            limitUsers = Integer.parseInt(props.getProperty("limitUsers"));
        } catch (NumberFormatException ex) {
            logger.severe("Не указано или неверно указано значение ограничения подключаемых пользователей сервера. Будет установлено значение по умолчанию: 9999");
        }
        return limitUsers;
    }

    public int getnumberOfLastMessages() {
        int limitmes = 5;
        try {
            limitmes = Integer.parseInt(props.getProperty("numberOfLastMessages"));
        } catch (NumberFormatException ex) {
            logger.severe("Не указано или неверно указано значение количества последних сообщений. Будет установлено значение по умолчанию: 5");
        }
        return limitmes;
    }

    public String getLogFileName() {        
        if ((props.getProperty("logFile")).isEmpty()) {
            return "server.log";
        }
        return (props.getProperty("logFile"));        
    }    

}
