package threadedserver2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientThread extends Thread {

    private Socket socket;
    private PrintWriter out;
    private BufferedReader in;
    private String userName = "";        
    private static final Logger LOGGER = Logger.getLogger(ClientThread.class.getName());

    public ClientThread(Socket s) {        
        socket = s;
    }

    ClientThread(Socket s, boolean b) {
        try {
            socket = s;            
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            //out = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException ex) {
            Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public PrintWriter getOut() {
        return out;
    }

    public BufferedReader getIn(){
        return in;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }        
    
    @Override
    public void run() {
        try {            
            out = new PrintWriter(socket.getOutputStream(), true);                                    
            ThreadedServer2.sendHistory(out);            
            while (true) {
                ThreadedServer2.sendUsersList();                
                String line = in.readLine();
                 if ((line == null)||(line.contains(" bye"))) {
                    break;
                }                                        
                   line = timeString() + " " + line;
                   ThreadedServer2.addMessageToHistory(line); 
                   ThreadedServer2.sendMessage("m " + line);             
            }           
            close();
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }
    
    private String timeString(){
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm:ss");
         return sdf.format(new java.util.Date());
            
    }
  
    public synchronized void close() {
        try {
            ThreadedServer2.getClients().remove(this);
            ThreadedServer2.sendUsersList();
            in.close();
            out.close();
            socket.close();            
            LOGGER.info("Пользователь " + userName + " вышел");
            LOGGER.info("кол-во пользователей после выхода пльзователя " + userName + " = " + ThreadedServer2.getClients().size());
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }
}
