package threadedserver2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerManager implements Runnable {

    private BufferedReader in;
    private ThreadedServer2 thd;
    private static final Logger LOGGER = Logger.getLogger(ServerManager.class.getName());
    public ServerManager(ThreadedServer2 thd) {
        this.thd = thd;
    }

    @Override
    public void run() {
        in = new BufferedReader(new InputStreamReader(System.in));
        String str = "";
        try {
            while (true) {
                System.out.print("Enter command > ");

                str = in.readLine();
                
                if ("exit".equals(str.trim())) {
                    break;
                }
                if ("list".equals(str.trim())) {
                    thd.list();
                }
                if ((str.contains(" "))&&("kill".equals(str.trim().substring(0, 4)))) {
                    thd.kill(str);
                }
                if ("help".equals(str.trim())) {
                    thd.help();
                }

            }
            close();
            thd.exit();
            
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }

    private void close() {
        try {
            System.out.println("closed");
            in.close();
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }
}
