/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package threadedserver2;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author top
 */
public class MessageStore {

    private  List<String> listLastMessages = new ArrayList<>();
    
    private PrintWriter out;
    
    public MessageStore(){
    
    }

    public List<String> getListLastMessages() {
        return listLastMessages;
    }
    

    public MessageStore(PrintWriter out) {
        this.out = out;
    }

    public   void addMessage(String str) {
        
        if ((str != null) && (str.length() > 0)) {
            listLastMessages.add(str);
        }
    }
    
    public   void delMessage(int index) {      
            listLastMessages.remove(index);        
    }
    public void sendListMessages() {        
            for (String string : listLastMessages) {
            out.println(string);
        }
    }
}
