package threadedserver2;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import log.LogClass;
import props.*;

public class ThreadedServer2 {

    private static List<ClientThread> clients = new ArrayList<>();
    private static MessageStore mesStore = new MessageStore();
    
    private static final Logger LOGGER = Logger.getLogger(ThreadedServer2.class.getName());
    private static PropsClass propsCl = new PropsClass();

    public static void main(String[] args) {
        LogClass.setFileLogger();
        new ThreadedServer2().run();
    }

    public static List<ClientThread> getClients() {
        return clients;
    }

    public static synchronized void sendMessage(String message) {
        /*
         *Выводим сообщения для каждого клиента, который находится в аррэйлист
         */
        for (ClientThread clientThread : clients) {
            clientThread.getOut().println(message);
        }
    }

    public static synchronized void sendUsersList() {
        String str = "UL";
        int i = 0;
        for (ClientThread clientThread : clients) {
            i++;
            str = str + clientThread.getUserName();
            if (i < clients.size()) {
                str = str + "#";
            }

        }
        sendMessage(str);

    }

    public static synchronized void addMessageToHistory(String str) {

        if (mesStore.getListLastMessages().size() >= propsCl.getnumberOfLastMessages()) {
            mesStore.delMessage(0);

        }
        mesStore.addMessage(str);
    }

    public static synchronized void sendHistory(PrintWriter out) {
        List<String> tempListMes = mesStore.getListLastMessages();

        if (tempListMes.size() > 0) {
            for (String string : tempListMes) {
                out.println("m " + string);
            }
        }

    }

    public void exit() {        
        sendMessage("server stopped");
        LOGGER.warning("server stopped");
        System.exit(0);
    }

    public void list() {
        int i = 0;
        for (ClientThread clientThread1 : clients) {
            System.out.println("User №" + ++i + " " + clientThread1.getUserName());
        }

    }

    public void kill(String param) {               
        ClientThread clientThread = null;
        String[] params = param.split("kill ");                       
        for (ClientThread clientTh : clients) {
            if (params[1].equals(clientTh.getUserName())) {                
                clientThread = clientTh;                                            
            } 
        }
        if (clientThread != null){
            clientThread.getOut().println(" bye");
            LOGGER.info("Пользователь " + params[1] + " был удален");
        }else {
            System.out.println("Пользователь " + params[1] + " не найден");
        }

    }

    public void help() {
        String help = "exit - отключить всех клиентов и завершить свою работу.\n"
                + "list — вывести в консоль список всех подключенных клиентов.\n"
                + "kill <имя пользователя> - разорвать соединение с указанным пользователем\n"
                + "help - выводит это сообщение.";
        System.out.println(help);

    }

    private void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(propsCl.getServerPort());
            ServerManager svm = new ServerManager(this);
            new Thread(svm).start();
            while (true) {
                Socket s = serverSocket.accept();
                ClientThread clientThread = new ClientThread(s, false);
                clientThread.setUserName(clientThread.getIn().readLine());
                PrintWriter out = new PrintWriter(s.getOutputStream(), true);
                if (isUserDuplicate(clientThread.getUserName())) {
                    out.println("0Такой пользователь уже подключен");
                } else {
                    if (isLimitUsersReached()) {                        
                        out.println("1Превышен лимит пользователей.");                        
                    } else {                    
                        out.println("2");
                        clientThread.start();
                        clients.add(clientThread);
                        LOGGER.info("Вошел Пользователь " + clientThread.getUserName());
                        LOGGER.info("кол-во пользователей = " + clients.size());
                    }
                }
            }
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }

    
    private boolean isLimitUsersReached() {        
        return clients.size() + 1 > propsCl.getLimitUsers();
    }

    private boolean isUserDuplicate(String userName) {
        for (ClientThread clientThread : clients) {
            if (userName.equals(clientThread.getUserName())) {
                return true;
            }
        }
        return false;
    }
}
